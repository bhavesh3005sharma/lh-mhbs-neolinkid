package com.example.guardianapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.example.guardianapp.databinding.ActivitySplashBinding
import com.example.guardianapp.lib.AgencyConfig
import com.example.guardianapp.lib.ConnectMeVcx
import pl.brightinventions.slf4android.LogLevel

class SplashActivity : BaseActivity() {

    private val SPLASH_TIME_OUT: Long = 1000
    private val TAG = SplashActivity::class.qualifiedName
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // implemented view binding
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initSDK()

    }

    fun initSDK() {
        binding.tvMessageSplash.text = getString(R.string.started_sdk_init)
        Toast.makeText(this, R.string.started_sdk_init, Toast.LENGTH_SHORT).show()

        // build the config with staging agency and genesis pool values
        val config: ConnectMeVcx.Config = ConnectMeVcx.Config.builder()
                .withAgency(AgencyConfig.AGENCY_CONFIG_STAGING)
                .withGenesisPool(R.raw.pool)
                .withWalletName(Constants.WALLET_NAME)
                .withLogLevel(LogLevel.DEBUG)
                .withContext(this)
                .build()

        // start the initialization of the SDK
        ConnectMeVcx.init(config, binding.tvMessageSplash).handleAsync { res, err ->
            if (err == null) { // SDK successfully initialized
                SingletonClass.getInstance(applicationContext).isSDKInitialized = true
                binding.tvMessageSplash.text = getString(R.string.sdk_init_success)
                runOnUiThread {
                    Toast.makeText(this, R.string.sdk_init_success, Toast.LENGTH_SHORT).show()

                    // navigate to home fragment in dashboard activity
                    Handler().postDelayed({
                        startActivity(Intent(this, DashboardActivity::class.java))
                        finish()
                    }, SPLASH_TIME_OUT)
                }
            } else { // SDK not initialized
                SingletonClass.getInstance(applicationContext).isSDKInitialized = false
                binding.tvMessageSplash.text = getString(R.string.sdk_not_init)
                runOnUiThread { Toast.makeText(this, getString(R.string.sdk_not_init), Toast.LENGTH_SHORT).show() }

            }

        }

    }

}
