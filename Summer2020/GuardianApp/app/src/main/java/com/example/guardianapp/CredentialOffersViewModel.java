package com.example.guardianapp;

import android.app.Application;
import android.util.Log;

import com.example.guardianapp.db.Database;
import com.example.guardianapp.db.entity.Connection;
import com.example.guardianapp.db.entity.Credential;
import com.example.guardianapp.lib.Credentials;
import com.example.guardianapp.lib.Messages;
import com.example.guardianapp.lib.message.Message;
import com.example.guardianapp.lib.message.MessageState;
import com.example.guardianapp.lib.message.MessageType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


// todo handle errors
public class CredentialOffersViewModel extends AndroidViewModel {
    private static String TAG = "CredentialOffersViewModel";
    private final Database db;
    private MutableLiveData<List<Credential>> credentialOffers;

    public CredentialOffersViewModel(@NonNull Application application) {
        super(application);
        db = Database.getInstance(application);
    }

    public LiveData<List<Credential>> getCredentialOffers() {
        if (credentialOffers == null) {
            credentialOffers = new MutableLiveData<>();
        }
        loadCredentialOffers();
        return credentialOffers;
    }

    public void loadCredentialOffers() {
        Log.d(TAG, "loadCredentialOffers");
        Executors.newSingleThreadExecutor().execute(() -> {
            List<Credential> data = db.credentialsDao().getAll();
            credentialOffers.postValue(data);
        });
    }

    public SingleLiveData<Boolean> getNewCredentialOffers() {
        SingleLiveData<Boolean> data = new SingleLiveData<>();
        checkCredentialOffers(data);
        return data;
    }

    public SingleLiveData<Credential> acceptOffer(int offerId) {
        SingleLiveData<Credential> data = new SingleLiveData<>();
        acceptCredentialOffer(offerId, data);
        return data;
    }

    private void acceptCredentialOffer(int offerId, SingleLiveData<Credential> data) {
        Executors.newSingleThreadExecutor().execute(() -> {
            Credential offer = db.credentialsDao().getById(offerId);
            Connection connection = db.connectionDao().getById(offer.connectionId);
            Credentials.acceptOffer(connection.serialized, offer.serialized,
                    offer.messageId).handle((s, throwable) -> {
                        if (s != null) {
                            String s2 = Credentials.awaitStatusChange(s, MessageState.ACCEPTED);
                            long time = System.currentTimeMillis();
                            offer.serialized = s2;
                            Log.d(TAG,"serialized:"+s2);
                            offer.setAccepted(true);
                            offer.setNeedAction(false);
                            offer.setCurrentTimeInMS(time);
                            //data.postValue(offer);
                            db.credentialsDao().update(offer);
                        }
                        //loadCredentialOffers();
                        data.postValue(offer);
                        return null;
                    }
            );
        });
    }


    private void checkCredentialOffers(SingleLiveData<Boolean> liveData) {
        Executors.newSingleThreadExecutor().execute(() -> {
            List<Connection> connections = db.connectionDao().getAll();
            List<Credential> credOffers = db.credentialsDao().getAll();
            for (Credential credentialOffer : credOffers) {
                printLogs(TAG, "CRED offers printLogs:", credentialOffer.toString());
            }
            for (Connection c : connections) {

                Log.d(TAG, "connection:" + c.toString());
                Messages.getPendingMessages(c.serialized, MessageType.CREDENTIAL_OFFER).handle(
                        (res, throwable) -> {
                            Log.d(TAG, "message" + res);
                            if (throwable != null) {
                                throwable.printStackTrace();
                            }
                            if (res != null) {
                                for (Message message : res) {
                                    CredDataHolder holder = extractDataFromCredentialsOfferMessage(
                                            message);
                                    Log.d(TAG, "holder.id:" + holder.id);
                                    Log.d(TAG, "holder.attributes:" + holder.attributes);
                                    Log.d(TAG, "c.id:" + c.id);

                                    if (!db.credentialsDao().checkOfferExists(holder.attributes,
                                            c.id)) {
                                        Credentials.createWithOffer(c.serialized,
                                                UUID.randomUUID().toString(), holder.offer).handle(
                                                (co, err) -> {
                                                    Util.printLogs(TAG,"Credential Serialized",co);
                                                    Util.printLogs(TAG,"messageID",message.getUid());
                                                    if (err != null) {
                                                        err.printStackTrace();
                                                    } else {
                                                        Credential offer = new Credential();
                                                        offer.claimId = holder.id;
                                                        offer.name = holder.name;
                                                        offer.organizationName = c.name;
                                                        offer.connectionId = c.id;
                                                        offer.attributes = holder.attributes;
                                                        offer.setNumberOfAttributes(holder.numberOfAttributes);
                                                        offer.serialized = co;
                                                        offer.messageId = message.getUid();
                                                        offer.setNeedAction(true);
                                                        offer.setAccepted(false);
                                                        db.credentialsDao().insertAll(offer);
                                                    }
                                                    loadCredentialOffers();
                                                    return null;
                                                });
                                    } else {
                                        Log.d("CredentialOffers", "exists");
                                    }
                                }
                            }
                            loadCredentialOffers();
                            liveData.postValue(true);
                            return res;
                        });
            }
            //liveData.postValue(true);
            if(connections.size() == 0) {
                liveData.postValue(true);
            }
        });
    }

    private CredDataHolder extractDataFromCredentialsOfferMessage(Message msg) {
        try {
            JSONObject data = new JSONArray(msg.getPayload()).getJSONObject(0);
            String id = data.getString("claim_id");
            String name = data.getString("claim_name");
            JSONObject attributesJson = data.getJSONObject("credential_attrs");
            StringBuilder attributes = new StringBuilder();
            Iterator<String> keys = attributesJson.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = attributesJson.getString(key);
                attributes.append(String.format("%s: %s\n", key, value));
            }
            return new CredDataHolder(id, name, attributesJson.toString(), msg.getPayload(),attributesJson.length());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    static class CredDataHolder {
        String id;
        String name;
        String attributes;
        String offer;
        int numberOfAttributes;

        public CredDataHolder(String id, String name, String attributes, String offer, int numberOfAttributes) {
            this.id = id;
            this.name = name;
            this.attributes = attributes;
            this.offer = offer;
            this.numberOfAttributes = numberOfAttributes;
        }
    }

    public static void printLogs(String TAG, String message, Object obj) {
        String actualLog = obj.toString();
        if (actualLog.length() > 4000) {
            int chunkCount = actualLog.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= actualLog.length()) {
                    Log.v(TAG, message + "::chunk " + i + " of " + chunkCount + "::"
                            + actualLog.substring(4000 * i));
                } else {
                    Log.v(TAG, message + "::chunk " + i + " of " + chunkCount + "::"
                            + actualLog.substring(4000 * i, max));
                }
            }
        } else {
            Log.d(TAG, message + "::" + actualLog);
        }
    }
}
