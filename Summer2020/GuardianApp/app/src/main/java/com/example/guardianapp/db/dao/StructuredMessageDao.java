package com.example.guardianapp.db.dao;

import com.example.guardianapp.db.entity.StructuredMessage;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface StructuredMessageDao {

    @Query("SELECT * FROM structuredmessage")
    List<StructuredMessage> getAll();

    @Query("SELECT * FROM structuredmessage WHERE id = :id")
    StructuredMessage getById(int id);

    @Insert
    void insertAll(StructuredMessage... messages);

    @Query("SELECT EXISTS(SELECT * FROM structuredmessage WHERE (entry_id = :entryId AND connection_id = :connectionId))")
    boolean checkMessageExists(String entryId, int connectionId);

    @Update
    void update(StructuredMessage message);

    @Delete
    void delete(StructuredMessage message);
}
