package com.example.guardianapp.db.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.guardianapp.SingletonClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Credential implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "connection_id")
    public int connectionId;

    @ColumnInfo(name = "organization_name")
    public String organizationName;

    @ColumnInfo(name = "serialized")
    public String serialized;

    @ColumnInfo(name = "claim_id")
    public String claimId;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "attributes")
    public String attributes;

    @ColumnInfo(name = "message_id")
    public String messageId;

    // used in demo mode
    @ColumnInfo(name = "time_in_ms")
    private long currentTimeInMS;

    @ColumnInfo(name = "date_time_string")
    private String dateTimeString;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "number_of_attributes")
    private int numberOfAttributes;

    @ColumnInfo(name = "is_accepted")
    private Boolean isAccepted;

    @ColumnInfo(name = "need_action")
    private Boolean needAction;

    protected Credential(Parcel in) {
        id = in.readInt();
        connectionId = in.readInt();
        organizationName = in.readString();
        serialized = in.readString();
        claimId = in.readString();
        name = in.readString();
        attributes = in.readString();
        messageId = in.readString();
        currentTimeInMS = in.readLong();
        dateTimeString = in.readString();
        type = in.readString();
        numberOfAttributes = in.readInt();
        byte tmpIsAccepted = in.readByte();
        isAccepted = tmpIsAccepted == 0 ? null : tmpIsAccepted == 1;
        byte tmpNeedAction = in.readByte();
        needAction = tmpNeedAction == 0 ? null : tmpNeedAction == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(connectionId);
        dest.writeString(organizationName);
        dest.writeString(serialized);
        dest.writeString(claimId);
        dest.writeString(name);
        dest.writeString(attributes);
        dest.writeString(messageId);
        dest.writeLong(currentTimeInMS);
        dest.writeString(dateTimeString);
        dest.writeString(type);
        dest.writeInt(numberOfAttributes);
        dest.writeByte((byte) (isAccepted == null ? 0 : isAccepted ? 1 : 2));
        dest.writeByte((byte) (needAction == null ? 0 : needAction ? 1 : 2));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Credential> CREATOR = new Creator<Credential>() {
        @Override
        public Credential createFromParcel(Parcel in) {
            return new Credential(in);
        }

        @Override
        public Credential[] newArray(int size) {
            return new Credential[size];
        }
    };

    public JSONObject getJSONObject(){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(attributes);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public Credential() {
    }

    public void setDemoCredential(String name, String organizationName, long currentTimeInMS,
            String dateTimeString, String type, String attributes, int numberOfAttributes,
            Boolean isAccepted, Boolean needAction) {
        this.name = name;
        this.organizationName = organizationName;
        this.currentTimeInMS = currentTimeInMS;
        this.dateTimeString = dateTimeString;
        this.type = type;
        this.attributes = attributes;
        this.numberOfAttributes = numberOfAttributes;
        this.isAccepted = isAccepted;
        this.needAction = needAction;
    }

    /*public void setLiveCredential(String name, String organizationName, long currentTimeInMS,
            String dateTimeString, String type, String attributes, int numberOfAttributes,
            Boolean isAccepted, Boolean needAction) {
        this.name = name;
        this.organizationName = organizationName;
        this.currentTimeInMS = currentTimeInMS;
        this.dateTimeString = dateTimeString;
        this.type = type;
        this.attributes = attributes;
        this.numberOfAttributes = numberOfAttributes;
        this.isAccepted = isAccepted;
        this.needAction = needAction;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getSerialized() {
        return serialized;
    }

    public void setSerialized(String serialized) {
        this.serialized = serialized;
    }

    public String getClaimId() {
        return claimId;
    }

    public void setClaimId(String claimId) {
        this.claimId = claimId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setDateTimeString(String dateTimeString) {
        this.dateTimeString = dateTimeString;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public void setNumberOfAttributes(int numberOfAttributes) {
        this.numberOfAttributes = numberOfAttributes;
    }

    public void setAccepted(Boolean accepted) {
        isAccepted = accepted;
    }

    public void setNeedAction(Boolean needAction) {
        this.needAction = needAction;
    }

    public long getCurrentTimeInMS() {
        return currentTimeInMS;
    }

    public Boolean getAccepted() {
        return isAccepted;
    }

    public Boolean getNeedAction() {
        return needAction;
    }

    public void setCurrentTimeInMS(long currentTimeInMS) {
        this.currentTimeInMS = currentTimeInMS;
        this.dateTimeString = SingletonClass.df.format(new Date(currentTimeInMS));
    }

    public String getDateTimeString() {
        return dateTimeString;
    }

    public String getAttributes() {
        return attributes;
    }

    public int getNumberOfAttributes() {
        return numberOfAttributes;
    }

    @Override
    public String toString() {
        return "Credential{" +
                "id=" + id +
                ", connectionId=" + connectionId +
                ", organizationName='" + organizationName + '\'' +
                ", claimId='" + claimId + '\'' +
                ", name='" + name + '\'' +
                ", attributes='" + attributes + '\'' +
                ", messageId='" + messageId + '\'' +
                ", currentTimeInMS=" + currentTimeInMS +
                ", dateTimeString='" + dateTimeString + '\'' +
                ", type='" + type + '\'' +
                ", numberOfAttributes=" + numberOfAttributes +
                ", isAccepted=" + isAccepted +
                ", needAction=" + needAction +
                ", serialized='" + serialized + '\'' +
                '}';
    }
}
